import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.3/firebase-app.js'
import { getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.3/firebase-auth.js";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyBb2otSmjDAmumnN8g2t8nwK8kfpaZ6oLI",
    authDomain: "foro-cc0fd.firebaseapp.com",
    databaseURL: "https://foro-cc0fd-default-rtdb.firebaseio.com",
    projectId: "foro-cc0fd",
    storageBucket: "foro-cc0fd.appspot.com",
    messagingSenderId: "111446238613",
    appId: "1:111446238613:web:6de4306d51f8932cf7029c"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

console.log("Consola de pruebas...")

let correoRef = document.getElementById("inputEmail")
let passRef = document.getElementById("inputPassword")
let ingresarRef = document.getElementById("buttonLogIn")

ingresarRef.addEventListener("click", logIn)

const auth = getAuth();

function logIn(){

    if((correoRef.value != '') && (passRef.value != '')){
        signInWithEmailAndPassword(auth, correoRef.value, passRef.value)
        .then((userCredential) => {
            // Signed in
            const user = userCredential.user;
            console.log("Correcto")
            window.location.href = "../index.html"

            // ...
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
        });
    }
    else{
        alert("Revisar que los campos de usuario y contraseña esten completos.");
    }   
}