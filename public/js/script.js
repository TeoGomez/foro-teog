// Importar funciones de Firebase
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.3/firebase-app.js'
import { getDatabase, ref, set, push, onValue, get, child, query, orderByChild, orderByKey } from "https://www.gstatic.com/firebasejs/9.8.3/firebase-database.js";
import { getAuth, onAuthStateChanged, signOut } from "https://www.gstatic.com/firebasejs/9.8.3/firebase-auth.js"
// Para evitar problemas de localia
var correo;
var UID;
var NOMBRE;
// Configuracion firebase
const firebaseConfig = {
  apiKey: "AIzaSyBb2otSmjDAmumnN8g2t8nwK8kfpaZ6oLI",
  authDomain: "foro-cc0fd.firebaseapp.com",
  databaseURL: "https://foro-cc0fd-default-rtdb.firebaseio.com",
  projectId: "foro-cc0fd",
  storageBucket: "foro-cc0fd.appspot.com",
  messagingSenderId: "111446238613",
  appId: "1:111446238613:web:6de4306d51f8932cf7029c"
};

const app = initializeApp(firebaseConfig);
const database = getDatabase(app);
const auth = getAuth();
const user = auth.currentUser;
const databaseRef = ref(getDatabase());

//Referencias
let comentarioRef = document.getElementById("textId")
let buttonRef = document.getElementById("buttonComentarId")
let comentarioAreaRef = document.getElementById("comentarioArea")
let signOutRef = document.getElementById("signOutId")

signOutRef.addEventListener("click", signOutFuncion)
// Usuario inciado 
onAuthStateChanged(auth, (user) => {
  if (user) {
    const uid = user.uid;
    UID = uid;
    console.log("Usuario actual:" + uid);

    const email = user.email;
    correo=email;
    console.log(email)
    buttonRef.addEventListener("click", comentar) 
  } else {
    console.log("Ningun usuario detectado ")
    buttonRef.addEventListener("click", ventana)
  }
});


function ventana(){
  window.location.href = "./pages/ingresar.html"
}

function comentar(){
  let comentario = comentarioRef.value

  push(ref(database, 'comentarios/'), {
  
   msg: comentario
  })

  comentarioRef.value = ''
  console.log("Comentario realizado")
  location.reload() // Solucion un poco rara pero funciona (consultar con ema)
}

function signOutFuncion(){
  signOut(auth).then(() => {
    // Sign-out successful.
  }).catch((error) => {
    // An error happened.
  });
}

const queryMensajes = query(ref(database, 'comentarios/'),
orderByKey('msg'))


onValue(queryMensajes, (snapshot) => {
const lectura = snapshot.val();

snapshot.forEach((childSnapshot) => {
  const childKey = childSnapshot.key;
  const childData = childSnapshot.val().msg;
  console.log(childData)

  comentarioAreaRef.innerHTML +=`
<div class="card mb-3">
<div class="row g-0">
  <div class="col-md-4 container-img">
    <img src="./media/rickuser.jpg" class="img-fluid rounded-start" alt="...">
  </div>
  <div class="col-md-8">
    <div class="card-body">
      <h5 class="card-title">${correo}</h5>
      <p class="card-text">${childSnapshot.val().msg}</p>
      <p class="card-text"><small class="text-muted">Hace un momento</small></p>
    </div>
  </div>
</div>
</div>
`
  });
})

