import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.3/firebase-app.js'
import { getAuth, createUserWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.3/firebase-auth.js";
import { getDatabase, ref, set, push } from "https://www.gstatic.com/firebasejs/9.8.3/firebase-database.js";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyBb2otSmjDAmumnN8g2t8nwK8kfpaZ6oLI",
    authDomain: "foro-cc0fd.firebaseapp.com",
    databaseURL: "https://foro-cc0fd-default-rtdb.firebaseio.com",
    projectId: "foro-cc0fd",
    storageBucket: "foro-cc0fd.appspot.com",
    messagingSenderId: "111446238613",
    appId: "1:111446238613:web:6de4306d51f8932cf7029c"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

console.log("Consola de pruebas...")

let correoRef = document.getElementById("inputEmail")
let passRef = document.getElementById("inputPassword")
let buttonRef = document.getElementById("buttonSignIn")

let nombreRef = document.getElementById("inputNombre")
let localidadRef = document.getElementById("inputLocalidad")

buttonRef.addEventListener("click", signIn)

const auth = getAuth();
const database = getDatabase(app);

function signIn(){

    if((correoRef.value != '') && (passRef != '')){

        createUserWithEmailAndPassword(auth, correoRef.value, passRef.value)
        .then((userCredential) => {
            // Signed in
            const user = userCredential.user;
            console.log("Usuario: " + user + "ID: " + user.uid)
            console.log("Usuario creado correctamente")

            let nombre = nombreRef.value
            let localidad = localidadRef.value


            set(ref(database, 'usuarios/'+  user.uid), {

                Nombre:nombre,
                Localidad: localidad
        
            }).then(() => {
                window.location.href = "../pages/ingresar.html"
            })
           
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log("Código de error: " + errorCode + "Mensaje de error: " + errorMessage)
            
            if(errorCode == 'auth/email-already-in-use'){
                alert("Mail ya empleado por otro usuario")
            }
        });
    }
    else{
        alert("Revisar que los campos de usuario y contraseña esten completos")
    }
}
